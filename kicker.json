{
  "name": "Terraform",
  "description": "Manage your infrastructure with [Terraform](https://www.terraform.io)",
  "template_path": "templates/gitlab-ci-terraform.yml",
  "kind": "infrastructure",
  "prefix": "tf",
  "is_component": true,
  "variables": [
    {
      "name": "TF_IMAGE",
      "description": "the Docker image used to run Terraform CLI commands - **set the version required by your project**",
      "default": "registry.hub.docker.com/hashicorp/terraform:latest"
    },
    {
      "name": "TF_GITLAB_BACKEND_DISABLED",
      "type": "boolean",
      "description": "Set to disable [GitLab managed Terraform State](https://docs.gitlab.com/ee/user/infrastructure/iac/terraform_state.html)",
      "advanced": true
    },
    {
      "name": "TF_PROJECT_DIR",
      "description": "Terraform project root directory",
      "default": ".",
      "advanced": true
    },
    {
      "name": "TF_SCRIPTS_DIR",
      "description": "Terraform (hook) scripts base directory (relative to `$TF_PROJECT_DIR`)",
      "default": ".",
      "advanced": true
    },
    {
      "name": "TF_OUTPUT_DIR",
      "description": "Terraform output directory (relative to `$TF_PROJECT_DIR`).\n\n_Everything generated in this directory will be kept as job artifacts._",
      "default": "tf-output",
      "advanced": true
    },
    {
      "name": "TF_EXTRA_OPTS",
      "description": "Default Terraform extra options (applies to all Terraform commands)",
      "advanced": true
    },
    {
      "name": "TF_INIT_OPTS",
      "description": "Default Terraform extra [init options](https://developer.hashicorp.com/terraform/cli/commands/init)",
      "default": "-reconfigure",
      "advanced": true
    },
    {
      "name": "TF_WORKSPACE",
      "description": "Default Terraform project [workspace](https://developer.hashicorp.com/terraform/language/state/workspaces)",
      "advanced": true
    },
    {
      "name": "TF_PLAN_OPTS",
      "description": "Default Terraform extra [plan options](https://developer.hashicorp.com/terraform/cli/commands/plan)",
      "advanced": true
    },
    {
      "name": "TF_APPLY_OPTS",
      "description": "Default Terraform extra [apply options](https://developer.hashicorp.com/terraform/cli/commands/apply)",
      "advanced": true
    },
    {
      "name": "TF_DESTROY_OPTS",
      "description": "Default Terraform extra [destroy options](https://developer.hashicorp.com/terraform/cli/commands/destroy)",
      "advanced": true
    },
    {
      "name": "TF_APK_EXTRA_OPTS",
      "description": "Extra [ `apk add` options](https://www.mankier.com/8/apk) (`apk` is used to install `jq` and/or `curl` if necessary)",
      "advanced": true
    }
  ],
  "features": [
    {
      "id": "tfsec",
      "name": "tfsec",
      "description": "Detect security issues with [tfsec](https://github.com/tfsec/tfsec)",
      "enable_with": "TF_TFSEC_ENABLED",
      "variables": [
        {
          "name": "TF_TFSEC_IMAGE",
          "description": "tfsec docker image",
          "default": "registry.hub.docker.com/aquasec/tfsec-ci",
          "advanced": true
        },
        {
          "name": "TF_TFSEC_ARGS",
          "description": "tfsec [options and args](https://aquasecurity.github.io/tfsec/latest/guides/usage/)",
          "default": "."
        }
      ]
    },
    {
      "id": "trivy",
      "name": "trivy",
      "description": "Detect security issues with [trivy config](https://aquasecurity.github.io/trivy/latest/docs/scanner/misconfiguration/)",
      "disable_with": "TF_TRIVY_DISABLED",
      "variables": [
        {
          "name": "TF_TRIVY_IMAGE",
          "description": "trivy docker image",
          "default": "registry.hub.docker.com/aquasec/trivy",
          "advanced": true
        },
        {
          "name": "TF_TRIVY_ARGS",
          "description": "trivy config [options and args](https://aquasecurity.github.io/trivy/latest/docs/references/configuration/cli/trivy_config/)",
          "default": "."
        }
      ]
    },
    {
      "id": "checkov",
      "name": "checkov",
      "description": "Static code analysis tool for infrastructure-as-code",
      "enable_with": "TF_CHECKOV_ENABLED",
      "variables": [
        {
          "name": "TF_CHECKOV_IMAGE",
          "description": "checkov docker image",
          "default": "registry.hub.docker.com/bridgecrew/checkov",
          "advanced": true
        },
        {
          "name": "TF_CHECKOV_ARGS",
          "description": "checkov [options and args](https://www.checkov.io/2.Basics/CLI%20Command%20Reference.html)",
          "default": "--framework terraform",
          "advanced": true
        }
      ]
    },
    {
      "id": "tf-infracost",
      "name": "tf-infracost",
      "description": "Shows cloud cost estimates for infrastructure-as-code projects",
      "enable_with": "TF_INFRACOST_ENABLED",
      "variables": [
        {
          "name": "TF_INFRACOST_IMAGE",
          "description": "Infracost docker image",
          "default": "registry.hub.docker.com/infracost/infracost",
          "advanced": true
        },
        {
          "name": "TF_INFRACOST_ARGS",
          "description": "infracost [CLI options and args](https://www.infracost.io/docs/#usage)",
          "default": "breakdown"
        },
        {
          "name": "TF_INFACOST_USAGE_FILE",
          "description": "infracost [usage file](https://www.infracost.io/docs/usage_based_resources/#infracost-usage-file)",
          "default": "infracost-usage.yml"
        },
        {
          "name": "INFRACOST_API_KEY",
          "description": "the infracost API key",
          "secret": true
        }
      ]
    },
    {
      "id": "tf-tflint",
      "name": "tf-tflint",
      "description": "Analyse your Terraform code with [tflint](https://github.com/terraform-linters/tflint)",
      "disable_with": "TF_TFLINT_DISABLED",
      "variables": [
        {
          "name": "TF_TFLINT_IMAGE",
          "description": "Tflint docker image",
          "default": "ghcr.io/terraform-linters/tflint-bundle:latest",
          "advanced": true
        },
        {
          "name": "TF_TFLINT_ARGS",
          "description": "tflint extra [options and args](https://github.com/terraform-linters/tflint/#usage)",
          "default": "--enable-plugin=google --enable-plugin=azurerm --enable-plugin=aws --recursive"
        }
      ]
    },
    {
      "id": "tf-fmt",
      "name": "tf-fmt",
      "description": "Check your Terraform code with [tffmt](https://developer.hashicorp.com/terraform/cli/commands/fmt#usage)",
      "enable_with": "TF_FMT_ENABLED",
      "variables": [
        {
          "name": "TF_FMT_ARGS",
          "description": "terraform fmt extra [options](https://developer.hashicorp.com/terraform/cli/commands/fmt#usage)",
          "default": "-diff -recursive"
        }
      ]
    },
    {
      "id": "tf-validate",
      "name": "tf-validate",
      "description": "Check your Terraform code with [tfvalidate](https://developer.hashicorp.com/terraform/cli/commands/validate#usage)",
      "enable_with": "TF_VALIDATE_ENABLED",
      "variables": []
    },
    {
      "id": "tfdocs",
      "name": "terraform docs",
      "description": "Build Terraform documentation based on [terraform docs](https://terraform-docs.io/)",
      "enable_with": "TF_DOCS_ENABLED",
      "variables": [
        {
          "name": "TF_DOCS_IMAGE",
          "description": "[terraform docs](https://terraform-docs.io/) container image",
          "default": "quay.io/terraform-docs/terraform-docs:edge",
          "advanced": true
        },
        {
          "name": "TF_DOCS_EXTRA_OPTS",
          "description": "Extra [terraform docs options](https://terraform-docs.io/reference/terraform-docs/)",
          "advanced": true
        },
        {
          "name": "TF_DOCS_CONFIG",
          "description": "terraform docs [configuration file](https://terraform-docs.io/user-guide/configuration/) (relative to `$TF_PROJECT_DIR`)",
          "default": ".terraform-docs.yml",
          "advanced": true
        },
        {
          "name": "TF_DOCS_OUTPUT_DIR",
          "description": "terraform docs output directory (relative to `$TF_PROJECT_DIR`)",
          "default": "docs",
          "advanced": true
        }
      ]
    },
    {
      "id": "tfpublish",
      "name": "publish module",
      "description": "Publish a Terraform module to GitLab's [Terraform Module Registry](https://docs.gitlab.com/ee/user/packages/terraform_module_registry/)",
      "enable_with": "TF_PUBLISH_ENABLED",
      "variables": [
        {
          "name": "TF_PUBLISH_IMAGE",
          "description": "container image used to publish module",
          "default": "registry.hub.docker.com/curlimages/curl:latest",
          "advanced": true
        },
        {
          "name": "TF_MODULE_NAME",
          "description": "The module name. May not contain any spaces or underscores.",
          "default": "$CI_PROJECT_NAME",
          "advanced": true
        },
        {
          "name": "TF_MODULE_SYSTEM",
          "description": "The module system or provider (example: `local`, `aws`, `google`)",
          "default": "local"
        },
        {
          "name": "TF_MODULE_VERSION",
          "description": "The module version. It must be valid according to the [semantic versioning](https://semver.org/) specification.",
          "default": "$CI_COMMIT_TAG",
          "advanced": true
        },
        {
          "name": "TF_MODULE_FILES",
          "description": "Glob patterns matching files to include into the Terraform module (:warning: does not support double star)",
          "default": "*.tf *.tpl *.md"
        }
      ]
    },
    {
      "id": "review",
      "name": "Review",
      "description": "Dynamic review environments for your topic branches (see GitLab [Review Apps](https://docs.gitlab.com/ee/ci/review_apps/))",
      "enable_with": "TF_REVIEW_ENABLED",
      "variables": [
        {
          "name": "TF_REVIEW_EXTRA_OPTS",
          "description": "Terraform extra options for `review` env (applies to all Terraform commands) (only define to override default)",
          "advanced": true
        },
        {
          "name": "TF_REVIEW_INIT_OPTS",
          "description": "Terraform extra [init options](https://developer.hashicorp.com/terraform/cli/commands/init) for `review` env (only define to override default)",
          "advanced": true
        },
        {
          "name": "TF_REVIEW_WORKSPACE",
          "description": "Terraform project [workspace](https://developer.hashicorp.com/terraform/language/state/workspaces) for `review` env (only define to override default)",
          "advanced": true
        },
        {
          "name": "TF_REVIEW_PLAN_ENABLED",
          "description": "Set to enable separate Terraform plan job for `review` env.",
          "type": "boolean",
          "advanced": true
        },
        {
          "name": "TF_REVIEW_PLAN_OPTS",
          "description": "Terraform extra [plan options](https://developer.hashicorp.com/terraform/cli/commands/plan) for `review` env (only define to override default)",
          "advanced": true
        },
        {
          "name": "TF_REVIEW_APPLY_OPTS",
          "description": "Terraform extra [apply options](https://developer.hashicorp.com/terraform/cli/commands/apply) for `review` env (only define to override default)",
          "advanced": true
        },
        {
          "name": "TF_REVIEW_DESTROY_OPTS",
          "description": "Terraform extra [destroy options](https://developer.hashicorp.com/terraform/cli/commands/destroy) for `review` env (only define to override default)",
          "advanced": true
        },
        {
          "name": "TF_REVIEW_AUTOSTOP_DURATION",
          "description": "The amount of time before GitLab will automatically stop `review` environments",
          "default": "4 hours"
        }
      ]
    },
    {
      "id": "integration",
      "name": "Integration",
      "description": "A continuous-integration environment associated to your integration branch (`develop` by default)",
      "enable_with": "TF_INTEG_ENABLED",
      "variables": [
        {
          "name": "TF_INTEG_EXTRA_OPTS",
          "description": "Terraform extra options for `integration` env (applies to all Terraform commands) (only define to override default)",
          "advanced": true
        },
        {
          "name": "TF_INTEG_INIT_OPTS",
          "description": "Terraform extra [init options](https://developer.hashicorp.com/terraform/cli/commands/init) for `integration` env (only define to override default)",
          "advanced": true
        },
        {
          "name": "TF_INTEG_WORKSPACE",
          "description": "Terraform project [workspace](https://developer.hashicorp.com/terraform/language/state/workspaces) for `integration` env (only define to override default)",
          "advanced": true
        },
        {
          "name": "TF_INTEG_PLAN_ENABLED",
          "description": "Set to enable separate Terraform plan job for `integration` env.",
          "type": "boolean",
          "advanced": true
        },
        {
          "name": "TF_INTEG_PLAN_OPTS",
          "description": "Terraform extra [plan options](https://developer.hashicorp.com/terraform/cli/commands/plan) for `integration` env (only define to override default)",
          "advanced": true
        },
        {
          "name": "TF_INTEG_APPLY_OPTS",
          "description": "Terraform extra [apply options](https://developer.hashicorp.com/terraform/cli/commands/apply) for `integration` env (only define to override default)",
          "advanced": true
        },
        {
          "name": "TF_INTEG_DESTROY_OPTS",
          "description": "Terraform extra [destroy options](https://developer.hashicorp.com/terraform/cli/commands/destroy) for `integration` env (only define to override default)",
          "advanced": true
        },
        {
          "name": "TF_INTEG_AUTOSTOP_DURATION",
          "description": "The amount of time before GitLab will automatically stop the `integration` env",
          "default": "never"
        }
      ]
    },
    {
      "id": "staging",
      "name": "Staging",
      "description": "An iso-prod environment meant for testing and validation purpose on your production branch (`main` or `master` by default)",
      "enable_with": "TF_STAGING_ENABLED",
      "variables": [
        {
          "name": "TF_STAGING_EXTRA_OPTS",
          "description": "Terraform extra options for `staging` env (applies to all Terraform commands) (only define to override default)",
          "advanced": true
        },
        {
          "name": "TF_STAGING_INIT_OPTS",
          "description": "Terraform extra [init options](https://developer.hashicorp.com/terraform/cli/commands/init) for `staging` env (only define to override default)",
          "advanced": true
        },
        {
          "name": "TF_STAGING_WORKSPACE",
          "description": "Terraform project [workspace](https://developer.hashicorp.com/terraform/language/state/workspaces) for `staging` env (only define to override default)",
          "advanced": true
        },
        {
          "name": "TF_STAGING_PLAN_ENABLED",
          "description": "Set to enable separate Terraform plan job for `staging` env.",
          "type": "boolean",
          "advanced": true
        },
        {
          "name": "TF_STAGING_PLAN_OPTS",
          "description": "Terraform extra [plan options](https://developer.hashicorp.com/terraform/cli/commands/plan) for `staging` env (only define to override default)",
          "advanced": true
        },
        {
          "name": "TF_STAGING_APPLY_OPTS",
          "description": "Terraform extra [apply options](https://developer.hashicorp.com/terraform/cli/commands/apply) for `staging` env (only define to override default)",
          "advanced": true
        },
        {
          "name": "TF_STAGING_DESTROY_OPTS",
          "description": "Terraform extra [destroy options](https://developer.hashicorp.com/terraform/cli/commands/destroy) for `staging` env (only define to override default)",
          "advanced": true
        },
        {
          "name": "TF_STAGING_AUTOSTOP_DURATION",
          "description": "The amount of time before GitLab will automatically stop the `staging` env",
          "default": "never"
        }
      ]
    },
    {
      "id": "prod",
      "name": "Production",
      "description": "The production environment",
      "enable_with": "TF_PROD_ENABLED",
      "variables": [
        {
          "name": "TF_PROD_EXTRA_OPTS",
          "description": "Terraform extra options for `production` env (applies to all Terraform commands) (only define to override default)",
          "advanced": true
        },
        {
          "name": "TF_PROD_INIT_OPTS",
          "description": "Terraform extra [init options](https://developer.hashicorp.com/terraform/cli/commands/init) for `production` env (only define to override default)",
          "advanced": true
        },
        {
          "name": "TF_PROD_WORKSPACE",
          "description": "Terraform project [workspace](https://developer.hashicorp.com/terraform/language/state/workspaces) for `production` env (only define to override default)",
          "advanced": true
        },
        {
          "name": "TF_PROD_PLAN_ENABLED",
          "description": "Set to enable separate Terraform plan job for `production` env.",
          "type": "boolean",
          "default": "true",
          "advanced": true
        },
        {
          "name": "TF_PROD_PLAN_OPTS",
          "description": "Terraform extra [plan options](https://developer.hashicorp.com/terraform/cli/commands/plan) for `production` env (only define to override default)",
          "advanced": true
        },
        {
          "name": "TF_PROD_APPLY_OPTS",
          "description": "Terraform extra [apply options](https://developer.hashicorp.com/terraform/cli/commands/apply) for `production` env (only define to override default)",
          "advanced": true
        },
        {
          "name": "TF_PROD_DESTROY_OPTS",
          "description": "Terraform extra [destroy options](https://developer.hashicorp.com/terraform/cli/commands/destroy) for `production` env (only define to override default)",
          "advanced": true
        }
      ]
    }
  ],
  "variants": [
    {
      "id": "vault",
      "name": "Vault",
      "description": "Retrieve secrets from a [Vault](https://www.vaultproject.io/) server",
      "template_path": "templates/gitlab-ci-terraform-vault.yml",
      "variables": [
        {
          "name": "TBC_VAULT_IMAGE",
          "description": "The [Vault Secrets Provider](https://gitlab.com/to-be-continuous/tools/vault-secrets-provider) image to use",
          "default": "registry.gitlab.com/to-be-continuous/tools/vault-secrets-provider:latest",
          "advanced": true
        },
        {
          "name": "VAULT_BASE_URL",
          "description": "The Vault server base API url",
          "mandatory": true
        },
        {
          "name": "VAULT_OIDC_AUD",
          "description": "The `aud` claim for the JWT",
          "default": "$CI_SERVER_URL"
        },
        {
          "name": "VAULT_ROLE_ID",
          "description": "The [AppRole](https://www.vaultproject.io/docs/auth/approle) RoleID",
          "mandatory": true,
          "secret": true
        },
        {
          "name": "VAULT_SECRET_ID",
          "description": "The [AppRole](https://www.vaultproject.io/docs/auth/approle) SecretID",
          "mandatory": true,
          "secret": true
        }
      ]
    },
    {
      "id": "gcp-auth-provider",
      "name": "Google Cloud",
      "description": "Retrieves an [OAuth access token](https://developers.google.com/identity/protocols/oauth2) for the [Google Cloud Platform Provider for Terraform](https://registry.terraform.io/providers/hashicorp/google/latest/docs)",
      "template_path": "templates/gitlab-ci-terraform-gcp.yml",
      "variables": [
        {
          "name": "GCP_OIDC_AUD",
          "description": "The `aud` claim for the JWT token _(only required for [OIDC authentication](https://docs.gitlab.com/ee/ci/cloud_services/aws/))_",
          "default": "$CI_SERVER_URL",
          "advanced": true
        },
        {
          "name": "GCP_OIDC_ACCOUNT",
          "description": "Default Service Account to which impersonate with OpenID Connect authentication"
        },
        {
          "name": "GCP_OIDC_PROVIDER",
          "description": "Default Workload Identity Provider associated with GitLab to [authenticate with OpenID Connect](https://docs.gitlab.com/ee/ci/cloud_services/google_cloud/)"
        },
        {
          "name": "GCP_REVIEW_OIDC_ACCOUNT",
          "description": "Service Account to which impersonate with OpenID Connect authentication on `review` environment",
          "advanced": true
        },
        {
          "name": "GCP_REVIEW_OIDC_PROVIDER",
          "description": "Workload Identity Provider associated with GitLab to [authenticate with OpenID Connect](https://docs.gitlab.com/ee/ci/cloud_services/google_cloud/) on `review` environment",
          "advanced": true
        },
        {
          "name": "GCP_INTEG_OIDC_ACCOUNT",
          "description": "Service Account to which impersonate with OpenID Connect authentication on `integration` environment",
          "advanced": true
        },
        {
          "name": "GCP_INTEG_OIDC_PROVIDER",
          "description": "Workload Identity Provider associated with GitLab to [authenticate with OpenID Connect](https://docs.gitlab.com/ee/ci/cloud_services/google_cloud/) on `integration` environment",
          "advanced": true
        },
        {
          "name": "GCP_STAGING_OIDC_ACCOUNT",
          "description": "Service Account to which impersonate with OpenID Connect authentication on `staging` environment",
          "advanced": true
        },
        {
          "name": "GCP_STAGING_OIDC_PROVIDER",
          "description": "Workload Identity Provider associated with GitLab to [authenticate with OpenID Connect](https://docs.gitlab.com/ee/ci/cloud_services/google_cloud/) on `staging` environment",
          "advanced": true
        },
        {
          "name": "GCP_PROD_OIDC_ACCOUNT",
          "description": "Service Account to which impersonate with OpenID Connect authentication on `production` environment",
          "advanced": true
        },
        {
          "name": "GCP_PROD_OIDC_PROVIDER",
          "description": "Workload Identity Provider associated with GitLab to [authenticate with OpenID Connect](https://docs.gitlab.com/ee/ci/cloud_services/google_cloud/) on `production` environment",
          "advanced": true
        }
      ]
    },
    {
      "id": "aws",
      "name": "AWS",
      "description": "Sets the appropriate Assume Role with Web Identity configuration for the [AWS Provider for Terraform](https://registry.terraform.io/providers/hashicorp/aws/latest/docs#assume-role-with-web-identity-configuration-reference)",
      "template_path": "templates/gitlab-ci-terraform-aws.yml",
      "variables": [
        {
          "name": "AWS_OIDC_AUD",
          "description": "The `aud` claim for the JWT",
          "default": "$CI_SERVER_URL"
        },
        {
          "name": "AWS_OIDC_ROLE_ARN",
          "description": "Default IAM Role ARN associated with GitLab to [authenticate using OpenID Connect](https://docs.gitlab.com/ee/ci/cloud_services/aws/)",
          "advanced": true
        },
        {
          "name": "AWS_REVIEW_OIDC_ROLE_ARN",
          "description": "IAM Role ARN associated with GitLab to [authenticate using OpenID Connect](https://docs.gitlab.com/ee/ci/cloud_services/aws/) on `review` env _(only define to override default)_",
          "advanced": true
        },
        {
          "name": "AWS_INTEG_OIDC_ROLE_ARN",
          "description": "IAM Role ARN associated with GitLab to [authenticate using OpenID Connect](https://docs.gitlab.com/ee/ci/cloud_services/aws/) on `integration` env _(only define to override default)_",
          "advanced": true
        },
        {
          "name": "AWS_STAGING_OIDC_ROLE_ARN",
          "description": "IAM Role ARN associated with GitLab to [authenticate using OpenID Connect](https://docs.gitlab.com/ee/ci/cloud_services/aws/) on `staging` env _(only define to override default)_",
          "advanced": true
        },
        {
          "name": "AWS_PROD_OIDC_ROLE_ARN",
          "description": "IAM Role ARN associated with GitLab to [authenticate using OpenID Connect](https://docs.gitlab.com/ee/ci/cloud_services/aws/) on `production` env _(only define to override default)_",
          "advanced": true
        }
      ]
    }
  ]
}
