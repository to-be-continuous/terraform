#!/usr/bin/env bats
load "$BATS_LIBRARIES_DIR/bats-support/load.bash"
load "$BATS_LIBRARIES_DIR/bats-assert/load.bash"
load 'terraform-scripts'

function setup() {
  export AAA=aaa
  export BBB=bbb
  export CCC=ccc
  export DDD=ddd

  export scoped__AAA__if__VEGETABLE__equals__carrot="overridden aaa"
  export scoped__BBB__if__VEGETABLE__in__lettuce__potato="overridden bbb"
  export scoped__CCC__if__VEGETABLE__endswith_ic__ato="overridden ccc"
  export scoped__DDD__ifnot__VEGETABLE__defined="overridden ddd"
}

@test "scoped variables with none" {
  # GIVEN

  # WHEN
  unscope_variables

  # THEN
  assert_equal "$AAA" 'aaa'
  assert_equal "$BBB" 'bbb'
  assert_equal "$CCC" 'ccc'
  assert_equal "$DDD" 'overridden ddd'
}

@test "scoped variables with carrot" {
  # GIVEN
  export VEGETABLE=carrot

  # WHEN
  unscope_variables

  # THEN
  assert_equal "$AAA" 'overridden aaa'
  assert_equal "$BBB" 'bbb'
  assert_equal "$CCC" 'ccc'
  assert_equal "$DDD" 'ddd'
}

@test "scoped variables with potato" {
  # GIVEN
  export VEGETABLE=potato

  # WHEN
  unscope_variables

  # THEN
  assert_equal "$AAA" 'aaa'
  assert_equal "$BBB" 'overridden bbb'
  assert_equal "$CCC" 'overridden ccc'
  assert_equal "$DDD" 'ddd'
}

@test "scoped variables with TOMATO" {
  # GIVEN
  export VEGETABLE=TOMATO

  # WHEN
  unscope_variables

  # THEN
  assert_equal "$AAA" 'aaa'
  assert_equal "$BBB" 'bbb'
  assert_equal "$CCC" 'overridden ccc'
  assert_equal "$DDD" 'ddd'
}
